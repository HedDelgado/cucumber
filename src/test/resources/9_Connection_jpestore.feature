# language: en
Feature: Connection jpestore

	Scenario Outline: Connection jpestore
		Given Je suis un utilisateur pas connectée à son profil sur la page d'accueil jpetstore
		And Que j'ai cliqué sur le menu 'Sign in'
		And Que je suis arrivé sur la âge d'authentification
		When Je rentre mon<pseudo_utilisateur>
		And Je rentre mon <mdp>
		And Je clique sur le bouton 'Login'
		Then Mon authentification réussie et je vois le menu 'Sing Out' apparaître à la place de 'Sign In'
		And Je suis de retour sur la page d'accueil
		And Le message Welcome <pseudo> apparait à gauche au dessus des menus des espèces animales

		@ccc
		Examples:
		| mdp | pseudo | pseudo_utilisateur |
		| "admin" | "admin" | "admin" |

		@Hehe
		Examples:
		| mdp | pseudo | pseudo_utilisateur |
		| "j2ee" | "ABC" | "j2ee" |