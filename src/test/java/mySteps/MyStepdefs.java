package mySteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.Keys;

import static org.junit.jupiter.api.Assertions.*;

//driver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
public class MyStepdefs {

    WebDriver driver;

    @Given("Je suis un utilisateur pas connectée à son profil sur la page d'accueil jpetstore")
    public void jeSuisUnUtilisateurPasConnecteeASonProfilSurLaPageDAccueilJpetstore() {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://petstore.octoperf.com/actions/Catalog.action");
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='MainImageContent']")));
        WebElement SignIn = driver.findElement(By.xpath("//a[contains(text(),'Sign In')]"));
        assertTrue(SignIn.isDisplayed());
    }

    @And("Que j'ai cliqué sur le menu 'Sign in'")
    public void queJaiCLiqueSurLeMenuSignIn() {
        driver.findElement(By.xpath("//a[contains(text(),'Sign In')]")).click();
        WebElement LoginMessage = driver.findElement(By.xpath("//p[contains(text(),'Please enter your username and password.')]"));
        assertTrue(LoginMessage.isDisplayed());
    }

    @And("Que je suis arrivé sur la âge d'authentification")
    public void queJeSuisArriveSurPageAuthen() {
        driver.findElement(By.xpath("//p[contains(text(),'Username')]")).isDisplayed();
        driver.findElement(By.xpath("//a[@href='/actions/Account.action?newAccountForm=']"));
    }

    @When("Je rentre mon{string}")
    public void jeRentreMonPseudo(String string){
        WebElement ChampName = driver.findElement(By.xpath("//input[@name='username']"));
        ChampName.click();
        ChampName.clear();
        ChampName.sendKeys(string);
        String Name = ChampName.getAttribute("value");
        assertEquals(string, Name);
    }

    @And("Je rentre mon {string}")
    public void jeRentreMdp(String string){
        WebElement ChampPassword = driver.findElement(By.xpath("//input[@name='password']"));
        ChampPassword.click();
        ChampPassword.clear();
        ChampPassword.sendKeys(string);
        String Password = ChampPassword.getAttribute("value");
        assertEquals(string, Password);
    }

    @And("Je clique sur le bouton 'Login'")
    public void JeCliqueSurLeBoutonLogin () {
        driver.findElement(By.xpath("//input[@value='Login']")).click();
        driver.findElement(By.xpath("//div[@id='MainImageContent']")).isDisplayed();
    }

    @Then("Mon authentification réussie et je vois le menu 'Sing Out' apparaître à la place de 'Sign In'")
    public void MonAuthenReussi () {
        WebElement SignOut = driver.findElement(By.xpath("//a[@href='/actions/Account.action?signoff=']"));
        SignOut.isDisplayed();
    }

    @And("Je suis de retour sur la page d'accueil")
    public void RetourPageAccueil () {
        String UrlPageAccueil = driver.getCurrentUrl();
        assertEquals("https://petstore.octoperf.com/actions/Catalog.action", UrlPageAccueil);
    }

    @And("Le message Welcome {string} apparait à gauche au dessus des menus des espèces animales")
    public void MessageWelcomme (String string){
        WebElement Welcome = driver.findElement(By.xpath("//div[@id='WelcomeContent']"));
        String WelcomeMessage = Welcome.getText();
        String A = "Welcome " + string + "!";
        assertEquals(A, WelcomeMessage);
        driver.close();
    }


}
